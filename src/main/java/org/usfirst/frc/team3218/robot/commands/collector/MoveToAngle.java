package org.usfirst.frc.team3218.robot.commands.collector;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team3218.robot.Robot;
import org.usfirst.frc.team3218.robot.subsystems.Collector;

public class MoveToAngle extends Command {
    private final Collector.Angle angle;
    private static double count = 0;
    private static double percent = 1;
    private boolean ret;
    public MoveToAngle(Collector.Angle angle){
        requires(Robot.collector);
        this.angle = angle;
        ret = false;
        percent = 1;
        count = 0;
    }
    protected void initialize(){
    }

    protected void execute(){
        count++;
        if(count>8)
            count = 8;
        percent = count/8;
        SmartDashboard.putString("MoveToAngle: ", "Executing");
    }

    protected boolean isFinished(){
        return ret=Robot.collector.moveTo(angle, percent);
    }

    protected void end(){
        Robot.collector.moveArm(0);
        count = 0;
        SmartDashboard.putString("MoveToAngle: ", "Finished");
    }

    protected void interrupted(){
        if(ret)
        Robot.collector.moveArm(0);
    }
}
