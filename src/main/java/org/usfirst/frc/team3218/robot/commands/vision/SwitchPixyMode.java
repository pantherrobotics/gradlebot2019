package org.usfirst.frc.team3218.robot.commands.vision;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team3218.robot.Robot;
import org.usfirst.frc.team3218.robot.sensors.Pixy2;

public class SwitchPixyMode extends Command {
    private final Pixy2.Objective objective;
    public SwitchPixyMode(Pixy2.Objective objective){
        this.objective = objective;
    }

    protected void initialize(){
        Robot.vision.frontPixy.setObjective(objective);
        if(objective== Pixy2.Objective.BLOB_RECOGNITION) {
            Robot.vision.frontPixy.setBrightness(Pixy2.Brightness.MEDIUM);
            Robot.vision.frontPixy.setObjective(Pixy2.Objective.BLOB_RECOGNITION);
        }
        else {
            Robot.vision.frontPixy.setBrightness(Pixy2.Brightness.LOW);
            Robot.vision.frontPixy.setObjective(Pixy2.Objective.LINE_TRACKING);
        }
    }

    protected boolean isFinished(){
        return true;
    }
}
