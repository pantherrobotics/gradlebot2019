package org.usfirst.frc.team3218.robot.commands.climber;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team3218.robot.Robot;
import org.usfirst.frc.team3218.robot.subsystems.BackClimber;

public class BringToBottom extends Command {
    private static float speed=BackClimber.DEFAULT_SPEED;

    public BringToBottom(float speed){
        requires(Robot.backClimber);
        this.speed=speed;
    }


    protected boolean isFinished(){
        return (BackClimber.State.atTop==Robot.backClimber.extend(speed));
    }
}
