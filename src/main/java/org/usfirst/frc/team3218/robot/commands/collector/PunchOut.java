package org.usfirst.frc.team3218.robot.commands.collector;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team3218.robot.Robot;
import org.usfirst.frc.team3218.robot.subsystems.Lift.Gear;

public class PunchOut extends Command {


    protected void initialize(){
        Robot.collector.setShifter(Gear.HIGH);
    }

    protected void execute(){
        Robot.collector.setShifter(Gear.HIGH);
    }

    protected boolean isFinished(){
        return false;
    }


    protected void interrupted()
    {
        Robot.collector.setShifter(Gear.LOW);
    }

    protected void end(){
        Robot.collector.setShifter(Gear.LOW);
    }
}

