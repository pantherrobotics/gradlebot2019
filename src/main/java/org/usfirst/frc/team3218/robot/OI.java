package org.usfirst.frc.team3218.robot;

import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import org.usfirst.frc.team3218.robot.commands.Positions.*;
import org.usfirst.frc.team3218.robot.commands.climber.ManualClimber;
import org.usfirst.frc.team3218.robot.commands.collector.*;
import org.usfirst.frc.team3218.robot.commands.vision.SwitchPixyMode;
import org.usfirst.frc.team3218.robot.sensors.Pixy2;
import org.usfirst.frc.team3218.robot.subsystems.Collector;

public class OI {

    // heightController is Not an xbox controller
   /**
    public Joystick heightController = new Joystick(RobotMap.heightPort);

    JoystickButton highestG = new JoystickButton(heightController, 1);
    JoystickButton secondHighestG = new JoystickButton(heightController, 2);
    JoystickButton midG = new JoystickButton(heightController, 4);
    JoystickButton secondLowestG = new JoystickButton(heightController, 3);
    JoystickButton lowestG = new JoystickButton(heightController, 5);
    */
    public Joystick buttonBoard = new Joystick(RobotMap.buttonBoardPort);

    JoystickButton lowHeight = new JoystickButton(buttonBoard, 1);
    JoystickButton midHeight = new JoystickButton(buttonBoard, 2);
    JoystickButton highHeight = new JoystickButton(buttonBoard, 3);
    JoystickButton shuttleHeight = new JoystickButton(buttonBoard, 12);
    JoystickButton lowBall = new JoystickButton(buttonBoard, 4);
    JoystickButton midBall = new JoystickButton(buttonBoard, 5);
    JoystickButton highBall = new JoystickButton(buttonBoard, 6);
    JoystickButton ballCollect = new JoystickButton(buttonBoard, 11);


    // xbox is an xbox controller
    public XboxController xbox = new XboxController(RobotMap.xboxPort);

    public JoystickButton aButton = new JoystickButton(xbox, 1);
    JoystickButton bButton = new JoystickButton(xbox, 2);
    JoystickButton yButton = new JoystickButton(xbox, 4);
    public JoystickButton xButton = new JoystickButton(xbox, 3);
    JoystickButton lBumper = new JoystickButton(xbox, 5);
    public JoystickButton rBumper = new JoystickButton(xbox, 6);
    JoystickButton backButton = new JoystickButton(xbox, 7);
    public JoystickButton startButton = new JoystickButton(xbox, 8);
    JoystickButton leftJoyClick = new JoystickButton(xbox, 9);
    public JoystickButton rightJoyClick = new JoystickButton(xbox, 10);

    public double getLeftJoystickY() {
        return -applyDeadband(xbox.getY(Hand.kLeft));
    }

    public double getLeftJoystickX() {
        return applyDeadband(xbox.getX(Hand.kLeft));
    }

    public double getRightJoystickY() {
        return -applyDeadband(xbox.getY(Hand.kRight));
    }

    public double getRightJoystickX() {
        return applyDeadband(xbox.getX(Hand.kRight));
    }

    private static double applyDeadband(double originalNumber) {
        if (originalNumber < .175 && originalNumber > -.175)
            return 0;
        else
            return (originalNumber * originalNumber) * Math.signum(originalNumber);
    }

    // Have to make commands for the buttons
    public OI() {
        //Height controller
	/*	button1.whenActive(new Command());
		button2.whenActive(new Command());
		button3.whenActive(new Command());
		button4.whenActive(new Command());
		button5.whenActive(new Command());
		button6.whenActive(new Command());
		*/
	    yButton.whileHeld(new PullInCollector());
        aButton.whenPressed(new SwitchPixyMode(Pixy2.Objective.BLOB_RECOGNITION));
        rightJoyClick.whenPressed(new SwitchPixyMode(Pixy2.Objective.LINE_TRACKING));
        xButton.whileHeld(new TurnCollector(-.25));
        bButton.whileHeld(new TurnCollector(.25));
        backButton.whileHeld(new ManualClimber(-.5));
        startButton.whileHeld(new ManualClimber(.5));
        lBumper.whileHeld(new PunchOut());
        ballCollect.whileHeld(new BallCollectPosition());
        lowHeight.whileHeld(new LowHatch());
        midHeight.whileHeld(new MiddleHatch());
        highHeight.whileHeld(new HighHatch());
        shuttleHeight.whileHeld(new ShuttleBall());
        lowBall.whileHeld(new LowBall());
        midBall.whileHeld(new MiddleBall());
        highBall.whileHeld(new HighBall());

    }

}
