package org.usfirst.frc.team3218.robot.commands.climber;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team3218.robot.Robot;

public class ManualClimber extends Command {

    private double speed = 0;

    public ManualClimber(double speed) {
        requires(Robot.backClimber);
        SmartDashboard.putNumber("climb speed", speed);
        this.speed = speed;
    }

    public void execute() {
        if (speed >= 0)
            Robot.backClimber.extend((float)speed);
        else
            Robot.backClimber.retract((float)-speed);
    }


    protected boolean isFinished() {
        return false;
    }

}
