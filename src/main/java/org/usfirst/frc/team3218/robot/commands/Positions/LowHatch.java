package org.usfirst.frc.team3218.robot.commands.Positions;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.usfirst.frc.team3218.robot.Robot;
import org.usfirst.frc.team3218.robot.commands.collector.MoveToAngle;
import org.usfirst.frc.team3218.robot.commands.lift.MoveToHeight;
import org.usfirst.frc.team3218.robot.subsystems.Collector;
import org.usfirst.frc.team3218.robot.subsystems.Lift;

public class LowHatch extends CommandGroup {

    public LowHatch(){
        addParallel(new MoveToHeight(Lift.Height.lowHatch));
        addParallel(new MoveToAngle(Collector.Angle.SQUARE_COLLECTOR));
    }


    protected void end() {
        Robot.collector.moveArm(0);
        Robot.lift.stop();
    }


}
