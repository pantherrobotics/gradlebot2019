package org.usfirst.frc.team3218.robot.commands.Positions;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.usfirst.frc.team3218.robot.commands.collector.MoveToAngle;
import org.usfirst.frc.team3218.robot.commands.lift.MoveToHeight;
import org.usfirst.frc.team3218.robot.subsystems.Collector;
import org.usfirst.frc.team3218.robot.subsystems.Lift;

public class PullInCollector extends CommandGroup {

    public PullInCollector(){
        addParallel(new MoveToHeight(Lift.Height.lowHatch));
        addParallel(new MoveToAngle(Collector.Angle.STARTING_POSITION));
    }
}
