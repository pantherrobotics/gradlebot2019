package org.usfirst.frc.team3218.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.revrobotics.*;
import edu.wpi.first.wpilibj.Counter;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PWM;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team3218.robot.Robot;
import org.usfirst.frc.team3218.robot.RobotMap;
import org.usfirst.frc.team3218.robot.commands.collector.CollectOrEject;
import org.usfirst.frc.team3218.robot.commands.collector.TurnCollector;

/**
 * need to incorporate forward angle limit again
 */
public class Collector extends Subsystem {
    public  CANSparkMax collectorArm = new CANSparkMax(RobotMap.armCollectorMotorPort, CANSparkMaxLowLevel.MotorType.kBrushless);
    private WPI_TalonSRX ballCollector = new WPI_TalonSRX(RobotMap.ballCollectorMotorPort);
    private DigitalInput encoder = new DigitalInput(RobotMap.armEncoderPort);
    private DigitalInput ballLimitSwitch= new DigitalInput(RobotMap.ballLimitSwitchPort);
    private DoubleSolenoid puncher = new DoubleSolenoid(RobotMap.puncherPortLow,RobotMap.puncherPortHigh);
    private static final int SAFE_TO_LIFT_ANGLE = 0;
    private static final int REVERSE_LIMIT_ANGLE= -70;
    private static final int FORWARD_LIMIT_ANGLE= 99;
    public static boolean safeToMove;
    public Counter counter = new Counter(Counter.Mode.kPulseLength);
    public Collector(){
        counter.setPulseLengthMode(50000);
        counter.setUpSource(encoder);
        counter.setSemiPeriodMode(true);
    }
    
    public enum Angle{
        STARTING_POSITION(-54),
        SQUARE_COLLECTOR(6),
        TOP_HEIGHT_HATCH(-10),
        WHEELS_ON_GROUND(99),
        CLIMB(108),
        BALL_SCORE(30);
        public final int value;
         Angle(int num){
            this.value = num;
        }
    }
    private double getEncoderPosition() {
        return counter.getPeriod();
    }

    public double getArmAngle() {
        return encoderToAngles(getEncoderPosition());
    }

    public void moveArm(double speed) {
        SmartDashboard.putBoolean("safe to move", speed<0&&safeToMoveBackward());
        if (speed<0&&safeToMoveBackward())
            collectorArm.set(speed);
        else
            speed = 0;

        if (speed==0)
            if(getArmAngle()<-54)
                collectorArm.set(0.02);
            else if(getArmAngle()>2.5)
                collectorArm.set(-0.02);
            else
                collectorArm.set(0);
    }

    public boolean safeToLift() {
        return getArmAngle()>=SAFE_TO_LIFT_ANGLE;
    }

    public boolean ballCheck() {
        return !ballLimitSwitch.get();
    }

    public void setCollector(double speed) {

        if(!ballCheck())
             ballCollector.set(ControlMode.PercentOutput, speed);
       else{
            if(speed>0)
                ballCollector.set(speed);
            else
                ballCollector.set(0);
        }

    }

    @Override
    protected void initDefaultCommand() {
        setDefaultCommand(new TurnCollector(0));
    }

    public void setShifter(Lift.Gear gear){
        if(gear== Lift.Gear.HIGH)
            puncher.set(DoubleSolenoid.Value.kForward);
        else
            puncher.set(DoubleSolenoid.Value.kReverse);
    }
    private double encoderToAngles(double val){
        return (val - 0.002709) * (90/(0.001742 - 0.002709));
    }

    private double semiproportional(double val){
        double speed = .022833*val;
        if(Math.abs(speed)<.15)
            speed = .15*Math.signum(speed);
        if(Math.abs(speed)>.8)
            speed = .8*Math.signum(speed);
        return speed;
    }

    private boolean movingThroughLift(Angle angle){
        if((getArmAngle()>0&&angle==Angle.STARTING_POSITION)||(getArmAngle()<-25))
            return true;
        else
            return false;
    }

    public boolean moveTo(Angle angle, double percent){
        double speed = percent *semiproportional(angle.value-getArmAngle());
        SmartDashboard.putNumber("collector speed", speed*percent);
        SmartDashboard.putNumber("collector percent", percent);
        safeToMove=Math.abs(angle.value-getArmAngle())<2
                ||
                (speed<0&&!safeToMoveBackward())|| (!Robot.lift.safeToMoveCollectorBetweenLift()&&movingThroughLift(angle));
        if(safeToMove) {
            moveArm(0);
            return true;
        }
        else{
            moveArm(speed);
            return false;
        }


    }

    private boolean safeToMoveBackward(){
        return getArmAngle()>REVERSE_LIMIT_ANGLE;
    }

    public boolean armClimb(){
        return moveTo(Angle.CLIMB,.5);
    }
}



