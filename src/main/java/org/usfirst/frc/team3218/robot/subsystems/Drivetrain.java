package org.usfirst.frc.team3218.robot.subsystems;



import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.AnalogGyro;
import org.usfirst.frc.team3218.robot.Robot;
import org.usfirst.frc.team3218.robot.RobotMap;
import org.usfirst.frc.team3218.robot.commands.drivetrain.DriveWithXbox;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;



public class Drivetrain extends Subsystem {

	// Put methods for controlling this subsystem
	// here. Call these from Commands.

	private CANSparkMax frontLeftMotor = new CANSparkMax(RobotMap.frontLeftMotorPort,MotorType.kBrushless);
	private CANSparkMax frontRightMotor = new CANSparkMax(RobotMap.frontRightMotorPort, MotorType.kBrushless);
	private CANSparkMax backLeftMotor = new CANSparkMax(RobotMap.backLeftMotorPort, MotorType.kBrushless);
	private CANSparkMax backRightMotor = new CANSparkMax(RobotMap.backRightMotorPort, MotorType.kBrushless);
	private AnalogGyro gyro = new AnalogGyro(RobotMap.GyroPort);

	private MecanumDrive robotDrive = new MecanumDrive(
			frontLeftMotor, frontRightMotor,
			backLeftMotor, backRightMotor);

	public Drivetrain() {
		robotDrive.setSafetyEnabled(false);
		frontRightMotor.setInverted(false);
		backLeftMotor.setInverted(false);
		backRightMotor.setInverted(false);
		frontLeftMotor.setInverted(false);
	}

	public void initDefaultCommand() {
		// Set the default command for a subsystem here.
		setDefaultCommand(new DriveWithXbox());
	}

	public void driveCartesian(double y, double x, double z) {
		if(Robot.oi.xbox.getName().equals("Controller (XBOX 360 For Windows)")) {
			SmartDashboard.putString("Xbox Controller", "Plugged In");
			SmartDashboard.putNumber("Drive y", y);
			SmartDashboard.putNumber("Drive x", x);
			SmartDashboard.putNumber("Drive z", z);
			robotDrive.driveCartesian(
					y * Robot.lift.liftFactor(),
					x * Robot.lift.liftFactor(),
					z * Robot.lift.liftFactor());
		}
		else
			SmartDashboard.putString("Xbox Controller", "Not Plugged In");

	}

	public void barryDrive(double y, double x, double z, double r, double f){
		if(Robot.oi.xbox.getName().equals("Controller (XBOX 360 For Windows)")) {
			SmartDashboard.putString("Xbox Controller", "Plugged In");
			frontLeftMotor.set((x + y + z + f)*Robot.lift.liftFactor());
			frontRightMotor.set((-(-x + y - z - f))*Robot.lift.liftFactor());
			backLeftMotor.set((-x + y + z - r)*Robot.lift.liftFactor());
			backRightMotor.set((-(x + y - z + r))*Robot.lift.liftFactor());
			SmartDashboard.putNumber("drive x", x*Robot.lift.liftFactor());
			SmartDashboard.putNumber("drive y", y*Robot.lift.liftFactor());
			SmartDashboard.putNumber("drive z", z*Robot.lift.liftFactor());
		}
		else
			SmartDashboard.putString("Xbox Controller", "Not Plugged In");
	}

	public double gyroAngle(){
		return gyro.getAngle();
	}
	public void calibrateGyro(){
		gyro.calibrate();
	}
}

