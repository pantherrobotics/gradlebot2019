package org.usfirst.frc.team3218.robot.subsystems;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import org.usfirst.frc.team3218.robot.Constants;
import org.usfirst.frc.team3218.robot.Robot;
import org.usfirst.frc.team3218.robot.RobotMap;
import org.usfirst.frc.team3218.robot.vision.Blob;
import org.usfirst.frc.team3218.robot.sensors.Lidar;
import org.usfirst.frc.team3218.robot.sensors.Pixy2;
import org.usfirst.frc.team3218.robot.vision.Vector;

/**
 *
 */

public class Vision extends Subsystem {
	public Pixy2 frontPixy;
	public  Vector[] frontFacingLines = new Vector[Constants.MAX_OBJECTS];
   	public Lidar sensor = new Lidar();
	private Blob[] balls = new Blob[Constants.MAX_OBJECTS];
	public Vector lastKnownLine;
	public boolean rememberedLine = false;
	public Blob mainBall;
	public boolean rememberedBall = false;
	private double[][] line = new double[4][5];
	private double[][] ball = new double[4][5];
	private int BALL_X_INDEX=0;
	private int BALL_Y_INDEX=1;
	private int BALL_WIDTH_INDEX=2;
	private int BALL_HEIGHT_INDEX=3;
	private int LINE_X0_INDEX=0;
	private int LINE_Y0_INDEX=1;
	private int LINE_X1_INDEX=2;
	private int LINE_Y1_INDEX=3;
    // Put methods for controlling this subsystem
    // here. Call these from Commands.

	public Vision() {
		frontPixy = new Pixy2(I2C.Port.kOnboard, RobotMap.pixy2AddressOne, Pixy2.Objective.LINE_TRACKING, "Front Facing Pixy");
	// 	frontPixy.getVersion();
	}
	
    public void initDefaultCommand() {

	}

    public void checkPixies() {

		if(frontPixy.getObjective()== Pixy2.Objective.LINE_TRACKING)
			setLines(frontPixy);
		else if(frontPixy.getObjective()==Pixy2.Objective.BLOB_RECOGNITION)
			setBlobs(frontPixy);

	}

	public void setLines(Pixy2 pix){
		rememberedBall = false;
		mainBall = null;
		frontFacingLines = (Vector[]) frontPixy.getObjects();
		if(!(frontFacingLines==null)){
			if(frontFacingLines.length>0&&!(frontFacingLines[0]==null)){
				lastKnownLine = frontFacingLines[0];
				SmartDashboard.putString("Front facing", "not null");
				rememberedLine = true;
				SmartDashboard.putNumber(frontPixy.getPixyName()+" Line X0", Robot.rollingAverage(line[LINE_X0_INDEX],lastKnownLine.x0));
				SmartDashboard.putNumber(frontPixy.getPixyName()+" Line X1",Robot.rollingAverage(line[LINE_X1_INDEX],lastKnownLine.x1));
				SmartDashboard.putNumber(frontPixy.getPixyName()+" Line Y0",Robot.rollingAverage(line[LINE_Y0_INDEX],lastKnownLine.y0));
				SmartDashboard.putNumber(frontPixy.getPixyName()+" Line Y1",Robot.rollingAverage(line[LINE_Y1_INDEX],lastKnownLine.y1));
			}
			else {
				SmartDashboard.putString("Front facing", "null");
				if(!rememberedLine)
					lastKnownLine = null;
				rememberedLine = false;

			}
		}
		else {
			lastKnownLine = null;
			rememberedLine = false;
		}

	}

	public void setBlobs(Pixy2 pix){
		rememberedLine = false;
		lastKnownLine = null;
		balls = (Blob[]) frontPixy.getObjects();
		if(!(balls==null)){
			if(balls.length>0&&!(balls[0]==null)){
				mainBall = balls[0];
				SmartDashboard.putString("Front facing", "not null");
				rememberedBall = true;
				SmartDashboard.putNumber(frontPixy.getPixyName()+" Ball X", Robot.rollingAverage(ball[BALL_X_INDEX],mainBall.getX()));
				SmartDashboard.putNumber(frontPixy.getPixyName()+" Ball Y", Robot.rollingAverage(ball[BALL_Y_INDEX],mainBall.getY()));
				SmartDashboard.putNumber(frontPixy.getPixyName()+" Ball Width", Robot.rollingAverage(ball[BALL_WIDTH_INDEX],mainBall.getWidth()));
				SmartDashboard.putNumber(frontPixy.getPixyName()+" Ball Height", Robot.rollingAverage(ball[BALL_HEIGHT_INDEX],mainBall.getHeight()));
			}
			else {
				SmartDashboard.putString("Front facing", "null");
				if(!rememberedBall)
					mainBall = null;
					rememberedBall = false;

			}
		}
		else {
			mainBall = null;
			rememberedBall = false;
		}
	}
}


