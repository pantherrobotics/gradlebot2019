package org.usfirst.frc.team3218.robot.commands.Positions;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.usfirst.frc.team3218.robot.commands.collector.MoveToAngle;
import org.usfirst.frc.team3218.robot.commands.lift.MoveToHeight;
import org.usfirst.frc.team3218.robot.subsystems.Collector;
import org.usfirst.frc.team3218.robot.subsystems.Lift;

public class ShuttleBall extends CommandGroup {
    public ShuttleBall(){
        addParallel(new MoveToHeight(Lift.Height.shuttleBall));
        addParallel(new MoveToAngle(Collector.Angle.BALL_SCORE));
    }
}
