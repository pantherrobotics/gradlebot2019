package org.usfirst.frc.team3218.robot.commands.lift;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team3218.robot.Interfaces.IRun;
import org.usfirst.frc.team3218.robot.Robot;

public class GuitarControl extends Command {


    public GuitarControl(){
        requires(Robot.lift);
    }
    protected void execute(){

        Robot.lift.liftGuitarControl();


    }


    protected boolean isFinished(){
        return false;
    }
}
