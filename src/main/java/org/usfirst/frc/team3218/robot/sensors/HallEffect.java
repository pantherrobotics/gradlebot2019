package org.usfirst.frc.team3218.robot.sensors;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class HallEffect extends DigitalInput implements Runnable {
    private boolean state;
    public HallEffect(int port){
        super(port);
    }

    @Override
    public void run(){
    while(true)
        SmartDashboard.putBoolean("Hall Effect lift",!get());
    }

}
