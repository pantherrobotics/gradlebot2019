package org.usfirst.frc.team3218.robot.commands.Positions;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.usfirst.frc.team3218.robot.commands.collector.MoveToAngle;
import org.usfirst.frc.team3218.robot.commands.lift.MoveToHeight;
import org.usfirst.frc.team3218.robot.subsystems.Collector;
import org.usfirst.frc.team3218.robot.subsystems.Lift;

public class HighHatch extends CommandGroup {

    public HighHatch(){
        addSequential(new MoveToHeight(Lift.Height.highHatch));
        addSequential(new MoveToAngle(Collector.Angle.TOP_HEIGHT_HATCH));
    }
}
