/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package org.usfirst.frc.team3218.robot;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import org.usfirst.frc.team3218.robot.commands.collector.CollectOrEject;
import org.usfirst.frc.team3218.robot.commands.collector.FindOrientation;

import org.usfirst.frc.team3218.robot.commands.vision.MoveToForwardLine;
import org.usfirst.frc.team3218.robot.subsystems.*;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.properties file in the
 * project.
 */

public class Robot extends TimedRobot {
    public static Drivetrain drive;
    public static Lift lift;
    public static Vision vision;
    public static OI oi;
    public static Collector collector;
    public static BackClimber backClimber;
    private static Thread[] sensors = new Thread[1];
    private static FindOrientation findOrientation = new FindOrientation();
    private PowerDistributionPanel panel = new PowerDistributionPanel();
    private Compressor compressor = new Compressor();

    /**
     * This function is run when the robot is first started up and should be
     * used for any initialization code.
     */
    @Override
    public void robotInit() {
        vision = new Vision();
        collector = new Collector();
        lift = new Lift();
        drive = new Drivetrain();
        backClimber = new BackClimber();
        oi = new OI();
        panel.clearStickyFaults();
        compressor.clearAllPCMStickyFaults();
        CameraServer.getInstance().startAutomaticCapture("cam1", 0);
        CameraServer.getInstance().startAutomaticCapture("cam2", 1);


        findOrientation.start();
        sensors[0] = new Thread(vision.frontPixy);
        SmartDashboard.putBoolean("MoveToHeightDone: ", true);
        collector.setShifter(Lift.Gear.LOW);
        Robot.drive.barryDrive(0, 0, 0, 0, 0);

    }

    @Override
    public void robotPeriodic() {
        Scheduler.getInstance().run();
        SmartDashboard.putNumber("Left Joystick Y Axis: ", oi.getLeftJoystickY());
        SmartDashboard.putNumber("Left Joystick X Axis: ", oi.getLeftJoystickX());
        SmartDashboard.putNumber("Right Joystick X Axis :", oi.getRightJoystickX());
        SmartDashboard.putBoolean("Lined Up", MoveToForwardLine.linedUp);
        SmartDashboard.putString("Lift Status", lift.liftStatus());
        SmartDashboard.putBoolean("Has Ball", collector.ballCheck());
        panel.clearStickyFaults();
        compressor.clearAllPCMStickyFaults();
        SmartDashboard.putNumber("Collector angle", collector.getArmAngle());
        SmartDashboard.putBoolean("Remembers Ball", vision.rememberedBall);
        SmartDashboard.putBoolean("Remembers Line", vision.rememberedLine);
        SmartDashboard.putBoolean("top", lift.atTop());
        SmartDashboard.putBoolean("bottom", lift.atBottom());
        SmartDashboard.putBoolean("Safe To Lift", collector.safeToLift());
        SmartDashboard.putBoolean("pressure switch", compressor.getPressureSwitchValue());
        SmartDashboard.putBoolean("safe to move collector", collector.safeToMove);
        SmartDashboard.putNumber("lift factor", lift.liftFactor());
        SmartDashboard.putNumber("backlift encoder", backClimber.getEncoderValue());
        SmartDashboard.putNumber("climber state", backClimber.getState().height);
        SmartDashboard.putNumber("climber height", backClimber.getHeight());
    }

    /**
     * This autonomous (along with the chooser code above) shows how to select
     * between different autonomous modes using the dashboard. The sendable
     * chooser code works with the Java SmartDashboard. If you prefer the
     * LabVIEW Dashboard, remove all of the chooser code and uncomment the
     * getString line to get the auto name from the text box below the Gyro
     *
     * <p>You can add additional auto modes by adding additional comparisons to
     * the switch structure below with additional strings. If using the
     * SendableChooser make sure to add them to the chooser code above as well.
     */

    @Override
    public void autonomousInit() {
        Scheduler.getInstance().run();
        teleopInit();
        new CollectOrEject().start();
        Robot.drive.barryDrive(0, 0, 0, 0, 0);
    }

    /**
     * This function is called periodically during autonomous.
     */
    @Override
    public void autonomousPeriodic() {
        Scheduler.getInstance().run();
    }

    @Override
    public void teleopInit() {

        if (!sensors[0].isAlive()) {
            sensors[0] = new Thread(vision.frontPixy);
            vision.frontPixy.open();
            sensors[0].start();
        }

        Scheduler.getInstance().run();
        new CollectOrEject().start();
        Robot.drive.barryDrive(0, 0, 0, 0, 0);

    }


    /**
     * This function is called periodically during operator control.
     */
    @Override
    public void teleopPeriodic() {
        Scheduler.getInstance().run();
    }

    /**
     * This function is called periodically during test mode.
     */
    @Override
    public void testPeriodic() {
    }

    @Override
    public void disabledInit() {
        Scheduler.getInstance().removeAll();
        collector.setShifter(Lift.Gear.LOW);
        vision.frontPixy.pause();
        panel.clearStickyFaults();
        compressor.clearAllPCMStickyFaults();
        Robot.drive.barryDrive(0, 0, 0, 0, 0);

    }

    @Override
    public void disabledPeriodic() {
        Scheduler.getInstance().removeAll();
        panel.clearStickyFaults();
        compressor.clearAllPCMStickyFaults();
        Robot.drive.barryDrive(0, 0, 0, 0, 0);
    }

    public static double rollingAverage(double[] set, double value) {
        double sum = 0;
        for (int x = 0; x < set.length - 1; x++) {
            set[x] = set[x + 1];
            sum += set[x + 1];
        }
        set[set.length - 1] = value;
        sum += value;
        return sum / set.length;
    }

    public static double getAverage(double[] set) {
        double sum = 0;
        for (double num : set)
            sum += num;
        return sum / set.length;
    }

}