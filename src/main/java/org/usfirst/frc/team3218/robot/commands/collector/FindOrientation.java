package org.usfirst.frc.team3218.robot.commands.collector;

import org.usfirst.frc.team3218.robot.Robot;


public class FindOrientation extends Thread {
    private boolean foundLift =false;
    @Override
    public void run(){
        while(!foundLift) {

            if(Robot.lift.atBottom()){
                Robot.lift.orient(Robot.lift.getEncoderPosition());
                foundLift=true;
            }

        }

    }

}
