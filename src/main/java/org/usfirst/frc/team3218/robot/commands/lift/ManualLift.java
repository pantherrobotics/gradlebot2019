package org.usfirst.frc.team3218.robot.commands.lift;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team3218.robot.Interfaces.IRun;
import org.usfirst.frc.team3218.robot.Robot;

public class ManualLift extends Command {

    private static float speed=0;
    public ManualLift(float speed){
        requires(Robot.lift);
        this.speed=speed;
    }
    protected void execute(){
            Robot.lift.setLift(speed);
    }


    protected boolean isFinished(){
        return false;
    }
}
