package org.usfirst.frc.team3218.robot.commands.collector;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team3218.robot.Robot;

public class TurnCollector extends Command {
    private final double speed;
   public TurnCollector(double speed){
       requires(Robot.collector);
       this.speed = speed;
   }

   protected void execute(){
       Robot.collector.moveArm(speed);
   }
    @Override
    protected boolean isFinished(){
        return false;
    }
}
