package org.usfirst.frc.team3218.robot.sensors;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team3218.robot.Interfaces.IUpdate;
import org.usfirst.frc.team3218.robot.vision.Blob;

public class Pixy extends I2C implements IUpdate {
    private static final int MAX_OBJECTS = 1;
    private static final int MAX_BYTES =14*MAX_OBJECTS+2;
    private static final double smoothingFactor = 0.5;
    byte[] pixyValues = new byte[MAX_BYTES];
    Blob[] blobs = new Blob[MAX_OBJECTS];

    public Pixy(Port port, int address) {
        super(port, address);
    }

    public void update(){
        readOnly(pixyValues,MAX_BYTES);
        for(Blob blob:blobs)
            blob.setUpdated(false);
        readObjects(findStart());

    }


    private int findStart() {
        int i = 0;
        //search through data array to find object
        while (littleEndianToBigEndian(pixyValues[i], pixyValues[i + 1]) != 0xaa55) {
            i++;
            //if no object is found within the data exit the loop
            if (i > MAX_BYTES - 2) {
                break;
            }
        }
        return i+=2;
    }

    private short littleEndianToBigEndian(byte one, byte two)
    {
        return (short) (((two & 0xff) << 8) | (one & 0xff));

    }

    private void readObjects(int i){
        if(i<MAX_BYTES-14 && littleEndianToBigEndian(pixyValues[i],pixyValues[i+1])==0xaa55)
        {
            for(;i < pixyValues.length-14; i+=14)
            {
                if(littleEndianToBigEndian(pixyValues[i],pixyValues[i+1]) == 0xaa55)
                {
                    //System.out.println("92");
                    //sets all variables for current object in for loop
                    short currentChecksum = littleEndianToBigEndian(pixyValues[i + 2],pixyValues[i + 3]);
                    short currentSig = littleEndianToBigEndian(pixyValues[i + 4],pixyValues[i + 5]);
                    short currentX = littleEndianToBigEndian(pixyValues[i + 6],pixyValues[i + 7]);
                    //System.out.println(currentX);
                    short currentY = littleEndianToBigEndian(pixyValues[i + 8],pixyValues[i + 9]);
                    //System.out.println(currentY);
                    short currentWidth = littleEndianToBigEndian(pixyValues[i + 10],pixyValues[i + 11]);
                    //System.out.println(currentWidth);
                    short currentHeight = littleEndianToBigEndian(pixyValues[i + 12],pixyValues[i + 13]);
                    //System.out.println(currentHeight);

                    //checksum for one object
                    //System.out.println("the sum is gonna get checked!");
                    if( currentChecksum == (currentSig + currentX + currentY + currentWidth + currentHeight) && (currentChecksum > 0 )){//make sure data is good
                        //System.out.println("it ran");
                        int tempInt = currentSig;
                        SmartDashboard.putNumber("sig" , currentSig);
                        SmartDashboard.putNumber("X" +tempInt, blobs[currentSig].getX());
                        SmartDashboard.putNumber("Y" +tempInt, blobs[currentSig].getY());
                        SmartDashboard.putNumber("Width"+tempInt, blobs[currentSig].getWidth());
                        SmartDashboard.putNumber("Height"+tempInt, blobs[currentSig].getHeight());
                        calculateAverage(currentX, currentY, currentWidth, currentHeight, blobs[currentSig]);
                        for(int j = 0; j<blobs.length; j++)
                        {
                            blobs[j].setExistence(blobs[j].getUpdated());
                        }
                    }//checksum if close
                }//check for object and successful parse if close
            }//for loop that segments object data close
        }//if that checks for object in data close
    }//if the array has any data check close


    public void calculateAverage(short X, short Y, short Width, short Height, Blob blob)
    {
        //System.out.println("Calculated Average");
        if(blob.getExistence())
        {
            blob.setHeight((short)(smoothingFactor*Height+(1-smoothingFactor)*blob.getHeight()-1));
            blob.setWidth((short)(smoothingFactor*(short)Width+(1-smoothingFactor)*blob.getWidth()-1));
            blob.setX((short)(smoothingFactor*(float)X+(1-smoothingFactor)*blob.getX()-1));
            blob.setY((short)(smoothingFactor*(float)Y+(1-smoothingFactor)*blob.getY()-1));
            blob.setUpdated(true);
        } else {
            blob.setHeight(Height);
            blob.setWidth(Width);
            blob.setX(X);
            blob.setY(Y);
            blob.setUpdated(true);
        }
        SmartDashboard.putNumber("PixyX", blob.getX());
        SmartDashboard.putNumber("PixyY", blob.getY());
        SmartDashboard.putNumber("PixyW", blob.getWidth());
        SmartDashboard.putNumber("PixyH", blob.getHeight());
    }

}
