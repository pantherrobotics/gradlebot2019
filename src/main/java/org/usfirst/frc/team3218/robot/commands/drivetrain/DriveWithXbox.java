package org.usfirst.frc.team3218.robot.commands.drivetrain;


import org.usfirst.frc.team3218.robot.Robot;
import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team3218.robot.commands.vision.MoveToForwardLine;
import org.usfirst.frc.team3218.robot.commands.vision.TrackOnBall;

/**
 *
 */
public class DriveWithXbox extends Command {

    public DriveWithXbox(){
        requires(Robot.drive);
    }

    protected void execute() {

            if(Robot.oi.aButton.get() && Robot.vision.rememberedLine)
                MoveToForwardLine.lineUp();
            else if(Robot.oi.rightJoyClick.get()&&Robot.vision.rememberedLine)
                MoveToForwardLine.lineUp();
            else
                Robot.drive.barryDrive(Robot.oi.getLeftJoystickY(),Robot.oi.getLeftJoystickX(),Robot.oi.getRightJoystickX(),0,0);

    }

    protected boolean isFinished(){
        return false;
    }
}
