package org.usfirst.frc.team3218.robot.commands.vision;


import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team3218.robot.Robot;

public class TrackOnBall {
    private static double x;
    private static double y;
    private static double width;
    private static double height;
    private final static int PIXY_X_FULL_RES = 315;
    private static boolean hasBall = false;
    public static void track(float signature){

        if(!(Robot.vision.mainBall==null)){
            x = Robot.vision.mainBall.getX();
            y = Robot.vision.mainBall.getY();
            width = Robot.vision.mainBall.getWidth();
            height = Robot.vision.mainBall.getHeight();
            hasBall = true;
            SmartDashboard.putNumber("signature num", Robot.vision.mainBall.getSignature());
            SmartDashboard.putBoolean("signature same", Robot.vision.mainBall.getSignature()==signature);
        }
        else{
            hasBall = false;
        }
        if(hasBall) {
            Robot.drive.barryDrive(Robot.oi.getLeftJoystickY(), Robot.oi.getLeftJoystickX(), (x - PIXY_X_FULL_RES / 2.0) / (1 * PIXY_X_FULL_RES), 0, 0);
        }
        else
            Robot.drive.barryDrive(Robot.oi.getLeftJoystickY(),Robot.oi.getLeftJoystickX(),Robot.oi.getRightJoystickX(),0,0);
    }
}
