package org.usfirst.frc.team3218.robot.commands.vision;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team3218.robot.Robot;
import org.usfirst.frc.team3218.robot.vision.Blob;
import org.usfirst.frc.team3218.robot.vision.Vector;

public class MoveToForwardLine{

    private static boolean end = false;
    public static boolean linedUp = false;
    public MoveToForwardLine() {

    }

    public static void lineUp() {
        double x1= SmartDashboard.getNumber("Front Facing Pixy Line X0",0)-6;
        double x0 = SmartDashboard.getNumber("Front Facing Pixy Line X1",0)-6;
        double y0=SmartDashboard.getNumber("Front Facing Pixy Line Y1",0);
        double y1=SmartDashboard.getNumber("Front Facing Pixy Line Y0",0);
        double f=0;
        double r=0;
        double deadband =1;
        SmartDashboard.putNumber("x1 difference", (x1)-(39+deadband));
        SmartDashboard.putNumber("x0 difference", (x0)-(39+deadband));
        if((x1)<39+deadband && (x1)>39-deadband) {
            f = 0;

        }
        else {
            //function f(x)= (-3.24675e^-5)x^2 + .01x + .0383116
           f = function(x1-37);

        }

        if((x0)<39+deadband && (x0)>39-deadband) {
            r = 0;

        }
        else {
            r = function(x0-39);

        }

        if((x1)<37+deadband && (x1)>39-deadband && (x0)<39+deadband && (x0)>39-deadband) {
            end = true;
            linedUp = true;
            Robot.drive.barryDrive(Robot.oi.getLeftJoystickY(),0,0,0,0);
        }
        else {
            linedUp=false;
            end=false;
            Robot.drive.barryDrive(Robot.oi.getLeftJoystickY(),0,0,f*1.3,r*1.3);
            SmartDashboard.putNumber("f",f);
            SmartDashboard.putNumber("r",r);

        }

    }

    private static double function(double x){
       double num= (0.0125*x);
       SmartDashboard.putNumber("num pre stuff", num);
       if(Math.abs(num)>.25)
           num=.25*Math.signum(num);

       if(Math.abs(num)<.075)
           num=.075*Math.signum(num);

       SmartDashboard.putNumber("num after stuff",num);
       return num;
    }

}
