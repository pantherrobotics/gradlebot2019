package org.usfirst.frc.team3218.robot.subsystems;

import com.revrobotics.*;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team3218.robot.commands.lift.GuitarControl;
import org.usfirst.frc.team3218.robot.commands.lift.ManualLift;
import org.usfirst.frc.team3218.robot.Robot;

import org.usfirst.frc.team3218.robot.RobotMap;

public class Lift extends Subsystem {

    public CANSparkMax liftSlave = new CANSparkMax(RobotMap.liftMotorPort2, CANSparkMaxLowLevel.MotorType.kBrushless);
    public CANSparkMax liftMaster = new CANSparkMax(RobotMap.liftMotorPort1, CANSparkMaxLowLevel.MotorType.kBrushless);
    private CANEncoder masterEncoder = liftMaster.getEncoder();
    private CANPIDController pidController = liftMaster.getPIDController();
    public DigitalInput masterHallReverse = new DigitalInput(RobotMap.bottomSwitchPort);
    public DigitalInput masterHallForward = new DigitalInput(RobotMap.topSwitchPort);

    private final double P = .03;
    private final double I = .00;
    private final double D = 0.000;
    private Gear gear = Gear.LOW;
    private static final double LOW_GEAR_RATIO = 1 / 24.0;
    private static final double HIGH_GEAR_RATIO = 1 / 3.7;
    private static final double pulleyCircumference = Math.PI * 2.632; //inches (exact pulley diameter found online)
    private static final double LOW_INCHES_PER_REV = LOW_GEAR_RATIO * pulleyCircumference;
    private static final double HIGH_INCHES_PER_REV = HIGH_GEAR_RATIO * pulleyCircumference;
    private static final float NEO_FREE_RPM=0;
    private static final float NEO_FREE_RPS=NEO_FREE_RPM/60;
    private static final float FREE_CLIMBER_SPEED=(float)LOW_INCHES_PER_REV*NEO_FREE_RPS;
    private static final double startingHeight = 27.75;//(inches) measured at the top of the bottom collector wheels when hatch panel is perpendicular to lift to the baseplate
    private static final double lowHatchHeight = 17.5 - startingHeight;
    private static final double lowBallHeight = 25.5 - startingHeight;
    private static final double shuttleBallHeight = 39.75 - startingHeight;
    private static final double[] heights = {lowHatchHeight + startingHeight, (lowHatchHeight + 28 + 6) + startingHeight-1, (lowHatchHeight + 56 + 6) + startingHeight, //hatch heights
            lowBallHeight+startingHeight, (lowBallHeight + 28)+startingHeight, (lowBallHeight + 56)+startingHeight, // ball heights
            shuttleBallHeight+startingHeight,27.89}; //Shuttle ball height
    private boolean knowsOrientation = false;
    private double bottomRev = 0;
    private static final int GUITAR_MANUAL_UP = 0;
    private static final int GUITAR_MANUAL_DOWN = 180;
    private static final double LIFT_MAX_SPEED = 1;
    private static final double LIFT_MIN_SPEED = 0.2;
    public static final double LIFT_HOLD_POWER = .018;

    public Lift() {
        liftMaster.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus0, 1);
        pidController.setP(P);
        pidController.setI(I);
        pidController.setD(D);
        pidController.setOutputRange(-1, 1);
    }

    /**
     * Pre-specified values for moving the lift.
     */
    public enum Height {
        lowHatch(heights[0]),
        lowBall(heights[3]),
        middleHatch(heights[1]),
        middleBall(heights[4]),
        highHatch(heights[2]),
        highBall(heights[5]),
        shuttleBall(heights[6]),
        ballCollect(heights[7]);
        public final double value;

        Height(double value) {
            this.value = value;
        }
    }

    /** Givese information for the two different gear ratios
     *
     */
    public enum Gear {
        LOW(LOW_INCHES_PER_REV),
        HIGH(HIGH_INCHES_PER_REV),
        OFF(0.0);
        public final double value;

        Gear(double value) {
            this.value = value;
        }

    }

    @Override
    protected void initDefaultCommand() {
        setDefaultCommand(new GuitarControl() );
    }




    /**
     * Sets all motors to zero.
     */
    public void stop() {
        liftMaster.set(0);
        liftSlave.set(0);
    }

    /**
     *
     * @return the encoder position from the master motor controller
     */
    public double getEncoderPosition() {
        return masterEncoder.getPosition();
    }

    /**
     *
     * @return the percentage to affect driving speed based on the height of the lift
     */
    public double liftFactor() {
        if ((hatchGrabberHeight() < Height.middleHatch.value-2)||atBottom())
            return 1;
        else if ((hatchGrabberHeight() < Height.highHatch.value)&&!atTop())
            return .7;
        else if ((hatchGrabberHeight() >= Height.highHatch.value )||atTop())
            return 0.4;
        else
            return 1;
    }

    /**
     * @deprecated
     * @return the currently selected gearbox ratio
     */
    public Gear getGearboxRatio() {
        return gear;
    }



    public boolean atBottom() {
        return !masterHallReverse.get();
    }


    public boolean atTop() {
        return !masterHallForward.get();
    }

    public void orient(double num) {
        bottomRev = num;
        knowsOrientation = true;
    }

    public double getLiftHeight() {
        if (knowsOrientation)
            return (getEncoderPosition() - bottomRev) * Gear.LOW.value;
        else
            return 0;
    }

    public void liftGuitarControl() {
        if (Robot.oi.xbox.getName().equals("Controller (XBOX 360 For Windows)")) {
            switch (Robot.oi.xbox.getPOV()) {
                case GUITAR_MANUAL_UP:
                    setLift(.42);
                    break;
                case GUITAR_MANUAL_DOWN:
                    setLift(-.42);
                    break;

                default:
                    if(getLiftHeight()<1) {
                        liftMaster.set(-LIFT_HOLD_POWER);
                        liftSlave.set(-LIFT_HOLD_POWER);
                    }
                    else if(getLiftHeight()<3){
                        liftMaster.set(0);
                        liftSlave.set(0);
                    }
                    else{
                        liftMaster.set(LIFT_HOLD_POWER);
                        liftSlave.set(LIFT_HOLD_POWER);
                    }
                    break;
            }
        }
    }

    private double semiProportional(double val) {
        double speed = val/15;
        if (Math.abs(speed) < LIFT_MIN_SPEED)
            speed = LIFT_MIN_SPEED * Math.signum(speed);
        if (Math.abs(speed) > LIFT_MAX_SPEED)
            speed = LIFT_MAX_SPEED * Math.signum(speed);
        if(speed<0)
            speed*=.575;
        return speed;
    }

    public boolean moveTo(Height height, double percent) {
        double speed = percent * semiProportional(height.value - hatchGrabberHeight());
        SmartDashboard.putNumber("speed", speed * percent);
        SmartDashboard.putNumber("percent", percent);
        if (Math.abs(height.value - hatchGrabberHeight()) > 0.25 && !(atBottom() && speed < 0) && !(atTop() && speed > 0) && knowsOrientation) {
            setLift(speed);
            setLift(speed);
            return false;
        } else {
            liftGuitarControl();
            return true;
        }
    }

    public double carriageHeight() {
        return getLiftHeight() * 1.8 + 1;
    }

    public double hatchGrabberHeight() {
        return carriageHeight() + 19.2;
    }

    public String liftStatus() {
        if (atTop())
            return "At Top";
        else if (atBottom())
            return "At Bottom";
        else
            return (hatchGrabberHeight() + " inches high");
    }

    public void setLift(double speed) {
        if (Robot.collector.safeToLift()) {
            if (atTop() && speed > 0)
                speed = LIFT_HOLD_POWER;
            if (atBottom() && speed < 0)
                speed = -LIFT_HOLD_POWER;
            SmartDashboard.putNumber("lift motor speed", speed);
            liftMaster.set(speed);
            liftSlave.set(speed);
        } else {
            if(getLiftHeight()<1) {
                liftMaster.set(-LIFT_HOLD_POWER);
                liftSlave.set(-LIFT_HOLD_POWER);
            }
            else if(getLiftHeight()<3){
                liftMaster.set(0);
                liftSlave.set(0);
            }
            else{
                liftMaster.set(LIFT_HOLD_POWER);
                liftSlave.set(LIFT_HOLD_POWER);
            }
        }

    }

    public boolean safeToMoveCollectorBetweenLift() {
        return (getLiftHeight()<1 && knowsOrientation)||atBottom();
    }


    public float inchesPerSecondToMotorPercentage(float inchesPerSecond){
        return inchesPerSecond/FREE_CLIMBER_SPEED;
    }
}
