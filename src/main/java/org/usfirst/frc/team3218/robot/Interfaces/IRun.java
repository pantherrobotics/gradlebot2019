package org.usfirst.frc.team3218.robot.Interfaces;

public interface IRun {
    public boolean isRunning();
    public void pause();
    public void reopen();

}
