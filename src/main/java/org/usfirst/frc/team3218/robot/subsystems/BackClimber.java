package org.usfirst.frc.team3218.robot.subsystems;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team3218.robot.RobotMap;
import org.usfirst.frc.team3218.robot.commands.climber.ManualClimber;

/**
 * The back lift that pushes underneath the robot to climb.
 */
public class BackClimber extends Subsystem {

    private static WPI_TalonSRX actuator=new WPI_TalonSRX(RobotMap.climberPort); // driver for the climber, encoder is in the last stage (after the gear ratio - meaning its not as necessary to know).

    private static final int RS775_PRO_FREE_RPM=18060; // free rotations per minute
    private static final int RS775_PRO_FREE_RPS=RS775_PRO_FREE_RPM/60; // free rotations per second
    private static final float GEAR_RATIO=1/50.0f;
    private static final int SPINDLE_SIZE=3; //in inches
    private static final float INCHES_PER_ROTATION=SPINDLE_SIZE*GEAR_RATIO;
    private static final float FREE_CLIMBER_SPEED=INCHES_PER_ROTATION*RS775_PRO_FREE_RPS; // in inches per second
    private static final double MAX_HEIGHT=19.9; //in inches
    private static final double INCHES_PER_TICK=1.0/650.0;
    public static final float DEFAULT_SPEED=.25f;



    public BackClimber(){
        actuator.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative);
        actuator.setSelectedSensorPosition(0);
    }

    /**
     * Gives details on the position of the back lift, normally the height and how it relates, values are in inches
     */
    public enum State {
        atBottom(MAX_HEIGHT),
        atTop(1),
        inMiddle(getHeight());

        public double height;
        State(double height){this.height=height;}
    }

    
    protected void initDefaultCommand(){
        setDefaultCommand(new ManualClimber(0));
    }

    /**
     *  Stops the climber from moving.
     */
    private void stop(){
        actuator.set(0);
    }

    /**Moves the climber out of the robot
     *
     * @return the state of the back lift
     */

    public State extend(float speed){
        if(getState()!=State.atBottom)
            actuator.set(speed);
        else {
            stop();
            speed=0;
        }
        return getState();
    }

    /**Moves the climber back inside of the robot
     *
     * @return the state of the back lift
     */
    public State retract(float speed){
        if(getState()!=State.atTop)
            actuator.set(-speed);
        else
            stop();
        return getState();
    }

    /**
     *
     * @return the current state (position) of the lift
     */
    public State getState(){
        if (getHeight()>=MAX_HEIGHT)
            return State.atBottom;
        else if (getHeight()<=0)
            return State.atTop;
        else
            return State.inMiddle;
    }

    /**
     *
     * @param inchesPerSecond the speed wanted to move at (in inches per second)
     * @return the approximate motor percentage for going the required inches per second
     */
    public float inchesPerSecondToMotorPercentage(float inchesPerSecond){
        return inchesPerSecond/FREE_CLIMBER_SPEED;
    }

    /**
     *
     * @return height in inches (how far down it has gone)
     */
    public static double getHeight(){
        return actuator.getSelectedSensorPosition()*INCHES_PER_TICK;
    }

    public double getEncoderValue(){ return actuator.getSelectedSensorPosition();}
}
