package org.usfirst.frc.team3218.robot.commands.Positions;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.usfirst.frc.team3218.robot.Robot;
import org.usfirst.frc.team3218.robot.commands.climber.BringToBottom;
import org.usfirst.frc.team3218.robot.commands.collector.CollectOrEject;
import org.usfirst.frc.team3218.robot.commands.collector.MoveToAngle;
import org.usfirst.frc.team3218.robot.commands.lift.ManualLift;
import org.usfirst.frc.team3218.robot.subsystems.Collector;

public class Climb extends CommandGroup {

    public Climb() {
        requires(Robot.lift);
        requires(Robot.backClimber);

        addParallel(new ManualLift(Robot.lift.inchesPerSecondToMotorPercentage(-3f)));
        addParallel(new BringToBottom(Robot.backClimber.inchesPerSecondToMotorPercentage(3f)));
        addParallel(new MoveToAngle(Collector.Angle.CLIMB));
    }



}
