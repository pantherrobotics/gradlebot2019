package org.usfirst.frc.team3218.robot;

public class RobotMap {

	//Analog Ports
	public static int GyroPort = 0;
	//Digital Ports
	public static int ballLimitSwitchPort = 7;
	public static int bottomSwitchPort = 1;
	public static int topSwitchPort = 2;
	//Can Ports
	public static int ballCollectorMotorPort = 11;
	public static int armCollectorMotorPort = 8;
	public static int liftMotorPort1 = 6;
	public static int liftMotorPort2 = 7;
	public static int climberPort = 3;


	public static int frontLeftMotorPort = 2;
	public static int frontRightMotorPort = 3;
	public static int backLeftMotorPort = 4;
	public static int backRightMotorPort = 5;

	// HID Ports
	public static int xboxPort = 3;
	public static int heightPort = 2;
	public static int buttonBoardPort = 1;

	//I2C Ports
	public static int pixy2AddressOne = 0x54;

	//0 = one of the climbers

	//top sol is
	//PCM Ports

	public static int puncherPortLow = 6;
	public static int puncherPortHigh = 7;


	//PWM Ports
	public static int armEncoderPort = 5;

}


