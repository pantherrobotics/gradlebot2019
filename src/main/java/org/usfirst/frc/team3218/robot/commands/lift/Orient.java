package org.usfirst.frc.team3218.robot.commands.lift;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team3218.robot.Robot;

public class Orient extends Command {

    protected void initialize(){
        Robot.lift.orient(Robot.lift.getEncoderPosition());
    }


    protected boolean isFinished(){
        return true;
    }
}
