package org.usfirst.frc.team3218.robot.commands.drivetrain;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team3218.robot.Robot;

public class DriveToBase extends Command {

    public DriveToBase(){
        requires(Robot.drive);
    }


    protected void execute(){
        SmartDashboard.putString("DriveToBase", "Executing");
        if(SmartDashboard.getNumber("Lidar distance",0)>60.96)
            Robot.drive.driveCartesian(.25,0,0);
        else
            Robot.drive.driveCartesian(0,0,0);
    }


    @Override
    protected boolean isFinished(){
        SmartDashboard.putString("DriveToBase","isFinished");
        return SmartDashboard.getNumber("Lidar distance",0)<60.96;
    }

    @Override
    protected void end(){
        SmartDashboard.putString("DriveToBase","Finished");
    }

    @Override
    protected void interrupted(){
        SmartDashboard.putString("DriveToBase","Interrupted");
    }
}
