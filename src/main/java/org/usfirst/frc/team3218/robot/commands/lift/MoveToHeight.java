package org.usfirst.frc.team3218.robot.commands.lift;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.usfirst.frc.team3218.robot.Robot;
import org.usfirst.frc.team3218.robot.subsystems.Lift;

public class MoveToHeight extends Command {
    private final Lift.Height height;
    private double count = 0;
    private static double percent = 1;
    public static boolean ret = false;
    public MoveToHeight(Lift.Height height){
        requires(Robot.lift);
        this.height = height;
        ret=false;
    }

    protected void initialize(){
    }

    protected void execute(){
        count++;
        if(count>5)
            count = 5;
        SmartDashboard.putNumber("count",count);
        percent = count/5;
        SmartDashboard.putNumber("MoveToHeight: ", height.value-Robot.lift.carriageHeight());
        SmartDashboard.putNumber("moveto",height.value);
    }

    protected boolean isFinished(){
         ret = Robot.lift.moveTo(height,percent);
        SmartDashboard.putBoolean("ret",ret);
        return ret;

    }

    protected void end(){
        Robot.lift.liftGuitarControl();
        SmartDashboard.putString("MoveToHeight: ", "Finished");
        percent = 1;
    }

    protected void interrupted(){
        if(ret)
            Robot.lift.liftGuitarControl();
    }
}
